package main

import (
	"encoding/xml"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"

	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
)

const tgApiToken = "1962449804:AAEpHwQfB7dtrBrkkLX295TBTvE4OkxNsOc"
const currentUrl = "https://slovodel.com/all-news.xml"
const flagFirst = "Путин"
const flagSecond = "Беглов"
const tgChatId = -1001141165661

func main() {
	oldLink := ""
	for {
		resp, err := http.Get(currentUrl)
		if err != nil {
			// log.Fatalln(err)
			continue
		}
		xmlFile, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			// log.Fatalln(err)
			continue
		}
		xmlFileString := string(xmlFile)
		xmlFileString = strings.Replace(xmlFileString, "content:encoded", "content", -1)
		xmlFile = []byte(xmlFileString)
		type Item struct {
			Link    []string `xml:"item>link"`
			Content []string `xml:"item>content"`
		}
		type Items struct {
			Items []Item `xml:"channel"`
		}
		dataXml := Items{}
		err = xml.Unmarshal(xmlFile, &dataXml)
		if err != nil {
			// fmt.Println("error: ", err)
			// return
			continue
		}
		paragraph := dataXml.Items[0].Content
		isContain := strings.Contains(paragraph[0], flagFirst) || strings.Contains(paragraph[0], flagSecond)
		if isContain && oldLink != dataXml.Items[0].Link[0] {
			newMessage := "В ленте новостей появилась новая публикация, содержащая интересующие вас ключевые слова. Ознакомиться: " + dataXml.Items[0].Link[0]
			bot, err := tgbotapi.NewBotAPI(tgApiToken)
			if err != nil {
				log.Panic(err)
			}
			msg := tgbotapi.NewMessage(int64(tgChatId), newMessage)
			if _, err = bot.Send(msg); err != nil {
				continue
			}
			oldLink = dataXml.Items[0].Link[0]
		}
		time.Sleep(30 * time.Second)
	}
}
